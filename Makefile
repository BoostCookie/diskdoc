.PHONY: install, uninstall, howto

howto:
	$(info Use `sudo make install` or `sudo make uninstall`)

install:
	@cp src/diskdoc.py /usr/local/bin/diskdoc

uninstall:
	rm /usr/local/bin/diskdoc

