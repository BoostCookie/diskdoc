# diskdoc

Check for disk errors using S.M.A.R.T. data and btrfs device stats.

Optionally add a config file `/etc/diskdoc/smartignore.yml` to ignore some S.M.A.R.T. errors. Here is an example:
```
- disk: "/dev/sdd"
  ignoremessage: |
    ATA Error Count: 6 (device log contains only the most recent five errors)
    Error 6 occurred at disk power-on lifetime: 21481 hours (895 days + 1 hours)
    Error 5 occurred at disk power-on lifetime: 21481 hours (895 days + 1 hours)
    Error 4 occurred at disk power-on lifetime: 21193 hours (883 days + 1 hours)
    Error 3 occurred at disk power-on lifetime: 21193 hours (883 days + 1 hours)
    Error 2 occurred at disk power-on lifetime: 21193 hours (883 days + 1 hours)
- disk: "/dev/sde"
  ignoremessage: |
    ATA Error Count: 1
    Error 1 occurred at disk power-on lifetime: 11481 hours (695 days + 2 hours)
```

Dependencies: `smartmontools`, `btrfs-progs`, `python3-yaml`

## Installation

### ArchLinux
```sh
# in an empty directory, get the PKGBUILD
curl -O "https://gitlab.com/BoostCookie/diskdoc/-/raw/master/PKGBUILD"
# install
makepkg -si
# uninstall
sudo pacman -Rsn backup-configs
```

### Other Systems
Make sure to have the dependency `rsync` installed.
```sh
# install
sudo make install
# uninstall
sudo make uninstall
```

## Run the script
`sudo diskdoc [--scrub] [--dry-run] [--test-error]`

Use `--scrub` to scrub all btrfs filesystems.

Use `--dry-run` for testing without starting any btrfs or smartmontools commands.

Use `--test-error` to make the program think an error occured even if there has not. (returncode 2)

## Send errors via telegram
Create the executable file `/etc/cron.daily/diskdoc` with the content
```sh
#!/bin/sh
chronic-telegram diskdoc
```
For this you need to install [chronic-telegram](https://gitlab.com/BoostCookie/chronic-telegram).

