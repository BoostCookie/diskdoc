#!/usr/bin/env python3

# Dependencies: smartmontools, btrfs-progs

from collections.abc import Iterable
import sys, subprocess, os, socket
from datetime import datetime, timezone

smartignorepath = "/etc/diskdoc/smartignore.yml"
logpath = "/var/log/diskdoc.log"
loglines_str = ""
printlog = True
dryrun = False

def log(line: str):
    global logpath
    global loglines_str
    global printlog

    if printlog:
        print(line)

    line = "{}\n".format(line)
    loglines_str += line

    with open(logpath, "a") as f:
        f.write(line)

def loglines(lines: Iterable[str]):
    for line in lines:
        log(line)

def insert_tabs(lines: Iterable[str], num_of_tabs: int = 1):
    for line in lines:
        yield "\t" * num_of_tabs + line

def process_args_str(args: Iterable[str]):
    ret_str = ""
    for arg in args:
        ret_str += "{} ".format(arg)
    return ret_str

def run_process(args):
    global dryrun
    if dryrun:
        return "", 0

    process = subprocess.run(args, capture_output=True, text=True)
    output = "{}\n{}".format(process.stdout, process.stderr).strip()
    return output, process.returncode

def scan_smart_devices():
    devices = []
    # S.M.A.R.T. scan
    log("Scanning for S.M.A.R.T. devices")
    smartctlscan_args = ["smartctl", "-n", "standby", "--scan"]
    log(process_args_str(smartctlscan_args))
    output, _ = run_process(smartctlscan_args)
    for device_infos in output.splitlines():
        device = device_infos.split(' ')[0]
        log("\tDetected {}".format(device))
        devices.append(device)
    return devices

def check_smart_device(device: str, ignorelist):
    erroroccured = False
    log("S.M.A.R.T. data for {}".format(device))

    ignoremessage = None
    for disk_imessage in ignorelist:
        if disk_imessage["disk"] != device:
            continue
        ignoremessage = disk_imessage["ignoremessage"].strip()

    # first look for errors only
    smartctl_errorsonly_args = ["smartctl", "-a", "-q", "errorsonly", "-n", "standby", device]
    output, _ = run_process(smartctl_errorsonly_args)
    # output is non empty --> S.M.A.R.T. has detected errors
    if output and output.strip() != ignoremessage:
        erroroccured = True
        log("ERRORS DETECTED FOR {}:".format(device))
        log(process_args_str(smartctl_errorsonly_args))
        loglines(insert_tabs(output.splitlines(), 1))
        log("All S.M.A.R.T. info for {}:".format(device))
    smartctl_args = ["smartctl", "-a", "-n", "standby", device]
    log("{}".format(process_args_str(smartctl_args)))
    output, _ = run_process(smartctl_args)
    loglines(insert_tabs(output.splitlines(), 1))

    return erroroccured

def scan_btrfs_paths():
    mountpaths = []
    # btrfs scan
    log("Scanning for btrfs filesystems")
    with open("/proc/mounts", "r") as mounts:
        # necessary because a filesystem with more than one subvolume can have multiple mountpoints
        devids = []
        for mount in mounts:
            mountinfos = mount.split(' ')
            if (mountinfos[2] == "btrfs") and (mountinfos[0] not in devids) and (mountinfos[1] not in mountpaths):
                log("\tDetected btrfs filesystem mounted at {}".format(mountinfos[1]))
                devids.append(mountinfos[0])
                mountpaths.append(mountinfos[1])
    return mountpaths

def check_btrfs_path(mountpath):
    erroroccured = False

    log("btrfs device stats for devices mounted at {}".format(mountpath))
    btrfs_devicestats_args = ["btrfs", "device", "stats", "-c", mountpath]
    output, returncode = run_process(btrfs_devicestats_args)
    if returncode != 0:
        erroroccured = True
        log("ERRORS DETECTED FOR {}".format(mountpath))
    log("{}".format(process_args_str(btrfs_devicestats_args)))
    loglines(insert_tabs(output.splitlines(), 1))

    return erroroccured

def btrfs_scrub(mountpath: str):
    erroroccured = False
    btrfs_scrub_args = ["btrfs", "scrub", "start", "-BdR", mountpath]
    output, _ = run_process(btrfs_scrub_args)
    # look for errors
    output_splitlines = output.splitlines()
    for line in output_splitlines:
        lineerrorsplit = line.split("_errors: ")
        if len(lineerrorsplit) != 1 and lineerrorsplit[1] != "0":
            erroroccured = True
            log("ERRORS DETECTED FOR {}".format(mountpath))
            break
    log(process_args_str(btrfs_scrub_args))
    loglines(insert_tabs(output_splitlines, 1))
    return erroroccured

def utcnow_iso():
    ret = datetime.now(timezone.utc)
    return "{}Z".format(ret.isoformat(timespec="seconds")[:-6])

def main():
    hostname = socket.gethostname()
    erroroccured = False
    ignorelist = []
    global smartignorepath
    if os.path.isfile(smartignorepath):
        with open(smartignorepath, 'r') as file:
            import yaml
            ignorelist = yaml.safe_load(file)

    doscrub = False
    global dryrun
    finished_at = ""
    # look for arguments
    for arg in sys.argv[1:]:
        if arg == "-s" or arg == "--scrub":
            doscrub = True
        elif arg == "-n" or arg == "--dry-run":
            dryrun = True
        elif arg == "--test-error":
            erroroccured = True
        #elif arg == "-h" or arg == "--help":
        else:
            print("usage: diskdoc [--scrub] [--dry-run] [--test-error]")
            return

    log("{} started at {} on {}".format(process_args_str(sys.argv), utcnow_iso(), hostname))

    devices = scan_smart_devices()
    log("")

    # get the S.M.A.R.T. data
    for device in devices:
        erroroccured = check_smart_device(device, ignorelist) or erroroccured
    # no need for log("") here, because smartctl's output ends with an empty line anyway

    mountpaths = scan_btrfs_paths()
    log("")

    # btrfs device stats
    for mountpath in mountpaths:
        erroroccured = check_btrfs_path(mountpath) or erroroccured
    log("")

    # btrfs scrub
    if doscrub:
        log("Doing scrubs on the btrfs filesystems")
        for mountpath in mountpaths:
            erroroccured = btrfs_scrub(mountpath) or erroroccured
        log("")

    finished_at = utcnow_iso()

    if erroroccured:
        log("ERRORS HAVE OCCURED")

    log("{} finished at {} on {}".format(process_args_str(sys.argv), finished_at, hostname))

    if erroroccured:
        sys.exit(2)

if __name__ == "__main__":
    main()
